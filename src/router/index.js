import Vue from 'vue';
import Router from 'vue-router';
import Episode from '@/components/views/Episode';
import Podcast from '@/components/views/Podcast';
import List from '@/components/views/List';

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'List',
			component: List
		},
		{
			path: '/podcast/:idPodcast',
			name: 'Podcast',
			component: Podcast
		},
		{
			path: '/podcast/:idPodcast/episode/:idEpisode',
			name: 'Episode',
			component: Episode
		}
	]
})
