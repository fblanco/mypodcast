import Vue from 'vue';
import axios from 'axios';
import VueLocalStorage from 'vue-ls';

Vue.use(VueLocalStorage);


const cacheable = true;
const expired_time = 86400000 ; //expiry 24 hour


const BASE_URL = 'https://itunes.apple.com';

export {getListPodcast, getPodcastDetail, getRSS};

axios.interceptors.request.use(request => {

    // Only cache GET requests
    if (request.method === 'get' && cacheable) {
        let url = request.url;

        // Append the params, I use jquery param but you can change to whatever you use
        if (request.params){
            url += '?' + Object.keys(request.params).map((k) => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');
        }

        const _cached = Vue.ls.get(url);

        if (_cached) {
            request.__fromCache = true;

            request.data = _cached;

            // Set the request adapter to send the cached response and prevent the request from actually running
            request.adapter = () => {
                return Promise.resolve({
                    data: _cached,
                    status: request.status,
                    statusText: request.statusText,
                    headers: request.headers,
                    config: request,
                    request: request
                });
            };
        }
    }

    return request;

}, error => Promise.reject(error));

axios.interceptors.response.use(response => {

    // if you dont want to cache a specific url, send a param `__cache = false`
    const isCacheable = !response.config.params || (response.config.params && response.config.params.__cache !== false);

    if (cacheable && isCacheable && !response.config.__fromCache) {
        let url = response.config.url;

        if (response.config.params){
        	url += '?' + Object.keys(response.config.params).map((k) => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');
        }  

        if (response.config.method === 'get') {
            Vue.ls.set(url, response.data, expired_time);
        }
    }

    return response;
}, error => Promise.reject(error));

function getListPodcast(numberOfElement) {
	numberOfElement = numberOfElement?numberOfElement:100;
  	const url = `${BASE_URL}/us/rss/toppodcasts/limit=${numberOfElement}/genre=1310/json`;
  	return axios.get(url).then(response => response.data.feed.entry).catch(e => console.error(`Error - getListPodcast(${numberOfElement}): ${e.message}`));
}

function getPodcastDetail(podcastId) {
	const url = `${BASE_URL}/lookup?id=${podcastId}`;
	return axios.get(url).then(response => response.data).catch(e => console.error(`Error - getPodcastDetail(${podcastId}): ${e.message}`));
}

function getRSS(url) {
	var request = `https://crossorigin.me/${url}`;
	return axios.get(request).then((response) => {
            var json = (response.data instanceof Object)?response.data:convertRSS2Json(response.data);

		    if(json.items.length===0 && Object.keys(json.feed).length===0){
		        console.error("Possibly the feed is malformed or not found. Try another service.");
			    //Servicio que convierte el feed en json
			    request = `https://feed2json.org/convert?url=${url}`;
				request = `https://crossorigin.me/${request}`;
				console.log(request);
		        return axios.get(request).then(response => response.data).catch(e => console.error(`Error - getEpisodes(${request}): ${e.message}`));
		    } else {
		    	if(!response.config.__fromCache){
		    		Vue.ls.set(request, json, expired_time);
		    	}
            	return json;
            }

    }).catch(e => console.error(`Error - getEpisodes(${request}): ${e.message}`));
}

function convertRSS2Json(xml){
    var parser = new DOMParser();
    var doc = parser.parseFromString(xml, "application/xml");

    var channel = doc.getElementsByTagName("channel");
    var details = {};
    if(channel.length!==0){
        details["title"] = getNodeValue(channel[0], "title"); 
        details["description"] = getNodeValue(channel[0], "description");
        details["author"] = getNodeValue(channel[0], "author");
        details["image"] = getAttrValue(channel[0], "image", "href");

    }


    var items=doc.getElementsByTagName("item");
    var itemsArr = [];
    for (var i=0;i<items.length;i++){
        var id = getNodeValue(items[i],"guid");
        id = id?id:i;//if not exist guid add index
            itemsArr.push(
                {
                    guid: id,
                    title : getNodeValue(items[i],"title"),
                    date_published : getNodeValue(items[i],"pubDate"),
                    description : getNodeValue(items[i],"description"),
                    url : getAttrValue(items[i],"enclosure", "url"),
                    summary : getNodeValue(items[i],"summary") || getNodeValue(items[i],"itunes:summary"),
                    duration: getNodeValue(items[i],"duration") || getNodeValue(items[i],"itunes:duration")
                }
            );
    }

    return {
        items:itemsArr,
        feed:details
    };
}

function getNodeValue(item, name){
    var node = item.getElementsByTagName(name)[0];
    return node?node.childNodes[0].nodeValue:undefined;
}

function getAttrValue(item, name, attr){
    var node = item.getElementsByTagName(name)[0];
    return node?node.getAttribute(attr):undefined;
}



