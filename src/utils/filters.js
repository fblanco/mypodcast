import Vue from 'vue';
import moment from 'moment'

Vue.filter('image', (item) => item?item:"https://pbs.twimg.com/profile_images/600060188872155136/st4Sp6Aw.jpg");
Vue.filter('formatDate', function(value, format) {
  if (value) {
  	format = format?format:'MM/DD/YYYY'
    return moment(String(value)).format(format);
  }
});

