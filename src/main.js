import Vue from 'vue';
import App from './App';
import router from './router';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';
import Vue2Filters from 'vue2-filters';
import Item from '@/components/commons/Item';
import './utils/filters';

Vue.use(Vue2Filters);
Vue.use(Buefy);

Vue.config.productionTip = false;
Vue.component("item", Item);

Vue.component('loading', {
	props:['loading'],
  	template: '<div class="myloader" v-show="loading">Loading...</div>'
});

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});

